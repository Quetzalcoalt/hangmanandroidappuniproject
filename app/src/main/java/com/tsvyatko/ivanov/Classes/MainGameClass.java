package com.tsvyatko.ivanov.Classes;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class MainGameClass {

    private static final String TAG = "MainGameClass";

    private ArrayList<String> words = new ArrayList<String>( Arrays.asList(
            "одеало"
            , "перефразирам"
            , "корщум"
            , "мармалад"
            , "тележрафия"
            , "фотография"
            , "котка"
            , "дърво"
            , "баница"
            , "книжарница"
            , "радиоаутопсия"));

    private ArrayList<String> wordsToChooseFrom;
    private int livesCounter;
    private String chosenWord;
    private String guessingWord;

    public MainGameClass (){
        wordsToChooseFrom = words;
        Log.d(TAG, wordsToChooseFrom.toString());
        resetLevel();
    }

    public void resetLevel(){
        if(wordsToChooseFrom.size() > 0){
            livesCounter = 4;
            chosenWord = getRandomNonRepeatingWord();
            guessingWord = convertWordToUnderscores(chosenWord);
        }else{
        }
    }

    public void clickedLetter(int index){
        char ch = '0';
        switch (index){
            case 0:         //А
                ch = 'а';
                break;
            case 1:         //Б
                ch = 'б';
                break;
            case 2:         //В
                ch = 'в';
                break;
            case 3:         //Г
                ch = 'г';
                break;
            case 4:         //Д
                ch = 'д';
                break;
            case 5:         //Е
                ch = 'е';
                break;
            case 6:         //Ж
                ch = 'ж';
                break;
            case 7:         //З
                ch = 'з';
                break;
            case 8:         //И
                ch = 'и';
                break;
            case 9:         //Й
                ch = 'й';
                break;
            case 10:         //К
                ch = 'к';
                break;
            case 11:         //Л
                ch = 'л';
                break;
            case 12:         //М
                ch = 'м';
                break;
            case 13:         //Н
                ch = 'н';
                break;
            case 14:         //О
                ch = 'о';
                break;
            case 15:         //П
                ch = 'п';
                break;
            case 16:         //Р
                ch = 'р';
                break;
            case 17:         //С
                ch = 'с';
                break;
            case 18:         //Т
                ch = 'т';
                break;
            case 19:         //У
                ch = 'у';
                break;
            case 20:         //Ф
                ch = 'ф';
                break;
            case 21:         //Х
                ch = 'х';
                break;
            case 22:         //Ц
                ch = 'ц';
                break;
            case 23:         //Ч
                ch = 'ч';
                break;
            case 24:         //Ш
                ch = 'ш';
                break;
            case 25:         //Щ
                ch = 'щ';
                break;
            case 26:         //Ъ
                ch = 'ъ';
                break;
            case 27:         //Ь
                ch = 'ь';
                break;
            case 28:         //Ю
                ch = 'ю';
                break;
            case 29:         //Я
                ch = 'я';
                break;
        }

        if(ch != '0'){
            checkIfChosenLetterExistInWord(ch);
        }else{
            //error
        }
    }

    private String getRandomNonRepeatingWord(){
        Random r = new Random();
        int number = r.nextInt(wordsToChooseFrom.size());
        String word = wordsToChooseFrom.get(number);
        wordsToChooseFrom.remove(number);
        return word;
    }

    private String convertWordToUnderscores(String word){
        String wordToReturn = "";
        for (char h: word.toCharArray()) {
            wordToReturn += "_ ";
        }
        //if wordToreturn is emptty Error!
        wordToReturn = wordToReturn.substring(0, wordToReturn.length() - 1);
        return wordToReturn;
    }

    public String getGuessingWord(){
        return guessingWord;
    }

    private void checkIfChosenLetterExistInWord(char ch){
        if(chosenWord.contains(String.valueOf(ch))){
            changeUndercaseToChar(ch);
            //
        }else{
            livesCounter--;
        }
    }

    private void changeUndercaseToChar(char ch){
        StringBuilder word = new StringBuilder(guessingWord);
        for(int i = 0; i < chosenWord.length(); i++){
            if(ch == chosenWord.charAt(i)){
                if(i != 0){
                    word.setCharAt(i * 2, ch);
                }else {
                    word.setCharAt(i, ch);
                }
            }
        }
        Log.wtf(TAG, word.toString());
        guessingWord = word.toString();
    }

    public int getLivesCounter(){
        return livesCounter;
    }

    public int getWordsArrayLength(){
        return wordsToChooseFrom.size();
    }

    public String getTheWord(){
        return chosenWord;

    }

    public boolean isGameWon(){
        if(chosenWord.equals(guessingWord.replace(" ", ""))){
            return true;
        }
        return false;
    }

    //random method for getting a word
    //lost life method
    //check if game ends
    //array of images ? onclick with value
    //lifes counter

    //clear method

}
