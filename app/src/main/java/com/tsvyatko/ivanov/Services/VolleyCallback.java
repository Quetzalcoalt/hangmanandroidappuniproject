package com.tsvyatko.ivanov.Services;

public interface VolleyCallback {
    void onError(String message);

    void onResponse(Object response);
}
