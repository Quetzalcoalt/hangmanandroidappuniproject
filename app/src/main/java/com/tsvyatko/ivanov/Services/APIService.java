package com.tsvyatko.ivanov.Services;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.tsvyatko.ivanov.Interfaces.Consts;
import com.tsvyatko.ivanov.Models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class APIService extends Application implements Consts {

    private static final String TAG = "APIService";
    private static APIService mInstance;
    private static Context mCtx;

    private APIService(Context context){
        mCtx = context;
    }
    public static synchronized APIService getInstance(Context context) {
        if(mInstance == null){
            mInstance = new APIService(context);
        }
        return mInstance;
    }

    public UserModel[] getUsers(final VolleyCallback callBack){

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                URL + GET_USERS,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        callBack.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onError(error.toString());
                    }
                }
            );

        VolleySingleton.getInstance(mCtx).addToRequestQueue(jsonArrayRequest, "jsonArrayRequest GET getAllUsers");
        return null;
    }

    public void registerUser(final String username, final String password, final VolleyCallback callBack){
        JSONObject request = new JSONObject();
        try{
            request.put("username",  username);
            request.put("password",  password);
        }catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectPostRequest = new JsonObjectRequest(
                Request.Method.POST,
                URL + CREATE_USER,
                request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onError(error.toString());
                    }
                }
        );
        VolleySingleton.getInstance(mCtx).addToRequestQueue(jsonObjectPostRequest, "jsonObjectPostRequest POST registerUser");
    }

    public void loginUser(final String username, final String password, final VolleyCallback callBack){

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                URL + LOGIN_USER + "?username=" + username + "&password=" + password,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            if(object.getBoolean("loginUser")){
                                callBack.onResponse(true);
                            }else{
                                callBack.onResponse(false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.wtf(TAG, error.toString());
                        callBack.onError(error.toString());
                    }
                }
        );

        VolleySingleton.getInstance(mCtx).addToRequestQueue(stringRequest, "jsonObjectRequest GET loginUser");
    }
}
