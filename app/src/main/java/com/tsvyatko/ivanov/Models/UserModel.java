package com.tsvyatko.ivanov.Models;

public class UserModel {

    private int id;
    private String username;

    public UserModel(int id, String username){
        this.id = id;
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }
}
