package com.tsvyatko.ivanov.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tsvyatko.ivanov.R;
import com.tsvyatko.ivanov.Services.APIService;
import com.tsvyatko.ivanov.Services.VolleyCallback;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    Button buttonRegister, buttonLogin, buttonPlaySolo;
    EditText editTextName, editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonLogin = findViewById(R.id.buttonLogin);
        buttonRegister = findViewById(R.id.buttonMakeAnAccount);
        buttonPlaySolo = findViewById(R.id.buttonPlaySolo);
        editTextName = findViewById(R.id.editTextName);
        editTextPassword = findViewById(R.id.editTextPassword);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIService.getInstance(getApplicationContext()).loginUser(
                        editTextName.getText().toString(),
                        editTextPassword.getText().toString(), new VolleyCallback() {
                    @Override
                    public void onError(String message) {
                        Log.wtf(TAG, message);
                        Toast.makeText(MainActivity.this,message, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onResponse(Object response) {
                        if((Boolean)response){
                            Intent intent = new Intent(MainActivity.this, GameMenuActivity.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(MainActivity.this, "Wrong credentials", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        buttonPlaySolo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GameActivity.class);
                startActivity(intent);
            }
        });
    }
}
