package com.tsvyatko.ivanov.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.tsvyatko.ivanov.Models.FriendModel;
import com.tsvyatko.ivanov.Adapters.FriendsAdapter;
import com.tsvyatko.ivanov.R;
import com.tsvyatko.ivanov.Services.APIService;
import com.tsvyatko.ivanov.Services.VolleyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MultiplayerActivity extends AppCompatActivity {

    private static final String TAG = "MultiplayerActivity";

    private RecyclerView recyclerViewFriends;
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager layoutManager;
    private DividerItemDecoration mDividerItemDecoration;

    private ArrayList<FriendModel> friends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiplayer);

        friends = new ArrayList<>();

        recyclerViewFriends = findViewById(R.id.recycledViewFriends);
        recyclerViewFriends.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerViewFriends.setLayoutManager(layoutManager);

        mDividerItemDecoration = new DividerItemDecoration(recyclerViewFriends.getContext(),
                LinearLayoutManager.HORIZONTAL);
        recyclerViewFriends.addItemDecoration(mDividerItemDecoration);

        //loading animaiton or something
        mAdapter = new FriendsAdapter(friends);
        recyclerViewFriends.setAdapter(mAdapter);
        Log.wtf(TAG, "calling doInBackground");
        getUsers();
    }

    private void getUsers(){
        APIService.getInstance(getApplicationContext()).getUsers(
                new VolleyCallback() {
                    @Override
                    public void onError(String message) {
                        Log.wtf(TAG, message);
                        //set error to true to displa something to the user
                    }

                    @Override
                    public void onResponse(Object response) {
                        ArrayList<FriendModel> fr = new ArrayList<>();
                        try {
                            JSONArray arr = new JSONArray(response.toString());

                            JSONObject o;
                            for(int i = 0; i < arr.length(); i++){
                                o = arr.getJSONObject(i);
                                friends.add(new FriendModel(o.getInt("id"), o.getString("username")));
                            }
                            mAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

}
