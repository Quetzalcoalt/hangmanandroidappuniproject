package com.tsvyatko.ivanov.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tsvyatko.ivanov.R;
import com.tsvyatko.ivanov.Services.APIService;
import com.tsvyatko.ivanov.Services.VolleyCallback;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";

    EditText editTextUsername, editTextPassword;
    Button buttonRegister;
    ProgressDialog nDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonRegister = findViewById(R.id.buttonRegister);

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

                nDialog = new ProgressDialog(RegisterActivity.this);
                nDialog.setMessage("Registering..");
                nDialog.setTitle("Please wait!");
                nDialog.setIndeterminate(true);
                nDialog.setCancelable(false);
                nDialog.show();

                APIService.getInstance(getApplicationContext()).registerUser(
                    editTextUsername.getText().toString(),
                    editTextPassword.getText().toString(),
                    new VolleyCallback(){
                        @Override
                        public void onError(String message) {
                            Log.wtf(TAG, message);
                            Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_LONG).show();
                            nDialog.dismiss();
                        }

                        @Override
                        public void onResponse(Object response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.toString());
                                nDialog.dismiss();
                                try {
                                    if(jsonObject.getBoolean("createdUser")){
                                        Toast.makeText(RegisterActivity.this, "User CREATED", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                        startActivity(intent);
                                    }else{
                                        Toast.makeText(RegisterActivity.this, "Username exists!", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                );
            }
        });
    }
}
