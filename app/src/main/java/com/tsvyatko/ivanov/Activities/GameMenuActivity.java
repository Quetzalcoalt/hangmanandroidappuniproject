package com.tsvyatko.ivanov.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.tsvyatko.ivanov.R;

public class GameMenuActivity extends AppCompatActivity {

    Button buttonSoloGame, buttonMultiplayerGame, buttonScoreBoard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_menu);

        buttonSoloGame = findViewById(R.id.buttonSolo);
        buttonMultiplayerGame = findViewById(R.id.buttonMultiplayer);
        buttonScoreBoard = findViewById(R.id.buttonScoreBoard);

        buttonSoloGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GameMenuActivity.this, GameActivity.class);
                startActivity(intent);
            }
        });

        buttonMultiplayerGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GameMenuActivity.this, MultiplayerActivity.class);
                startActivity(intent);
            }
        });
    }
}
