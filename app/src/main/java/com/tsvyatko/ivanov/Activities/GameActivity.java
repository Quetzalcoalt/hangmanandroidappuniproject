package com.tsvyatko.ivanov.Activities;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tsvyatko.ivanov.Classes.MainGameClass;
import com.tsvyatko.ivanov.R;

import java.util.ArrayList;
import java.util.List;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "GameActivity";

    MainGameClass game;
    int livesCounter;
    ImageView imageViewHangMan;
    TextView textViewGuessingWord;
    List<ImageView> characters;
    ImageView imageViewHeart1, imageViewHeart2, imageViewHeart3, imageViewHeart4;

    Integer[] images = {R.drawable.a1, R.drawable.a2, R.drawable.a3, R.drawable.a4, R.drawable.a5, R.drawable.a6
            ,R.drawable.a7,R.drawable.a8,R.drawable.a9,R.drawable.a10,R.drawable.a11,R.drawable.a12,R.drawable.a13,R.drawable.a14,R.drawable.a15
            ,R.drawable.a16,R.drawable.a17,R.drawable.a18,R.drawable.a19,R.drawable.a20,R.drawable.a21,R.drawable.a22,R.drawable.a23,R.drawable.a24
            ,R.drawable.a25,R.drawable.a26,R.drawable.a27,R.drawable.a28,R.drawable.a29,R.drawable.a30};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        textViewGuessingWord = findViewById(R.id.textViewGuessingWord);
        imageViewHangMan = findViewById(R.id.imageViewHangMan);
        imageViewHeart1 = findViewById(R.id.imageViewHeart1);
        imageViewHeart2 = findViewById(R.id.imageViewHeart2);
        imageViewHeart3 = findViewById(R.id.imageViewHeart3);
        imageViewHeart4 = findViewById(R.id.imageViewHeart4);

        characters = new ArrayList<>();

        for(int i = 0; i < 30; i++) {
            String iString = "imageView" + (i + 1);
            int resID = getResources().getIdentifier(iString, "id", getPackageName());
            ImageView imgView = findViewById(resID);
            imgView.setOnClickListener(this);
            imgView.setImageResource(images[i]);
            characters.add(imgView);
        }

        game = new MainGameClass();
        resetGame();
        textViewGuessingWord.setText(game.getGuessingWord());
    }

    @Override
    public void onClick(View v) {
        for(int i = 0; i < characters.size(); i++){
            ImageView im = characters.get(i);
            if(im.getId() == v.getId()){
                game.clickedLetter(i);
                removeOnClickListenerFromImage(im);
            }
        }
        textViewGuessingWord.setText(game.getGuessingWord());
        if(game.isGameWon()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setMessage("Do you want to play another one!").setTitle("You guess the word!").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }else{
            if(livesCounter != game.getLivesCounter()){
                livesCounter = game.getLivesCounter();
                changeHangManDrawableWhenLivesChanges(livesCounter);
                changeHeartsDrawablesWhenLivesChanges(livesCounter);
            }
            if(livesCounter == -1){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(false);
                builder.setMessage("Do you wanna play another one?").setTitle("Word was " + game.getTheWord()).setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        }
    }

    private void changeHangManDrawableWhenLivesChanges(int lives){
        switch (lives){
            case -1:
                imageViewHangMan.setBackgroundResource(R.drawable.game_over);
                break;
            case 0:
                imageViewHangMan.setBackgroundResource(R.drawable.one_live_left);
                break;
            case 1:
                imageViewHangMan.setBackgroundResource(R.drawable.two_lives_left);
                break;
            case 2:
                imageViewHangMan.setBackgroundResource(R.drawable.three_lives_left);
                break;
            case 3:
                imageViewHangMan.setBackgroundResource(R.drawable.four_lives_left);
                break;
            case 4:
                imageViewHangMan.setBackgroundResource(R.drawable.five_lives_left);
                break;
        }
    }

    private void removeOnClickListenerFromImage(ImageView img){
        grayOut(img);
        img.setOnClickListener(null);
    }

    private void grayOut(ImageView img){
        if(img.getTag() != "grayed"){
            img.setColorFilter(Color.argb(150,200,200,200));
            img.setTag("grayed");
        }
    }

    private void restoreColor(ImageView img){
        img.setColorFilter(null);
        img.setTag("");
    }

    private void resetGame(){
        //clear list or something
        textViewGuessingWord.setText(game.getGuessingWord());
        livesCounter = 4;
        changeHangManDrawableWhenLivesChanges(livesCounter);
        resetHeartsDrawables();
        for(ImageView im : characters){
            im.setOnClickListener(this);
            restoreColor(im);
        }
    }

    private void changeHeartsDrawablesWhenLivesChanges(int lives){
        switch (lives){
            case 0:
                imageViewHeart1.setBackgroundResource(R.drawable.empty_heart);
                break;
            case 1:
                imageViewHeart2.setBackgroundResource(R.drawable.empty_heart);
                break;
            case 2:
                imageViewHeart3.setBackgroundResource(R.drawable.empty_heart);
                break;
            case 3:
                imageViewHeart4.setBackgroundResource(R.drawable.empty_heart);
                break;
        }
    }

    private void resetHeartsDrawables(){
        imageViewHeart1.setBackgroundResource(R.drawable.full_heart);
        imageViewHeart2.setBackgroundResource(R.drawable.full_heart);
        imageViewHeart3.setBackgroundResource(R.drawable.full_heart);
        imageViewHeart4.setBackgroundResource(R.drawable.full_heart);
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    if(game.getWordsArrayLength() == 0){
                        Toast.makeText(GameActivity.this,"Sorry no more words!",Toast.LENGTH_LONG).show();
                        for(ImageView img: characters){
                            removeOnClickListenerFromImage(img);
                        }
                    }else {
                        game.resetLevel();
                        resetGame();
                    }
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    //intent the user to main window, or something.
                    break;
            }
        }
    };

}
