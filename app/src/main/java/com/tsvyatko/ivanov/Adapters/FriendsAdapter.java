package com.tsvyatko.ivanov.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tsvyatko.ivanov.Models.FriendModel;
import com.tsvyatko.ivanov.R;

import java.util.ArrayList;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {

    private static final String TAG = FriendsAdapter.class.getName();

    private ArrayList<FriendModel> dataset;

    public FriendsAdapter(ArrayList<FriendModel> dataset){
        this.dataset = dataset;
    }

    @NonNull
    @Override
    public FriendsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.friend_list_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FriendsAdapter.ViewHolder viewHolder, int i) {
        viewHolder.id = dataset.get(i).getId();
        viewHolder.textView.setText(dataset.get(i).getUsername());
    }

    @Override
    public int getItemCount() {
        return (dataset == null) ? 0 : dataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView textView;
        private int id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textView = itemView.findViewById(R.id.textViewFriend);
        }

        @Override
        public void onClick(View v) {
            Log.wtf(TAG, "clicked: " + id);
        }
    }
}
